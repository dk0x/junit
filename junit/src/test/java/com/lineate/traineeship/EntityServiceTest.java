package com.lineate.traineeship;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.internal.util.collections.Sets;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Enclosed.class)
public class EntityServiceTest {
    @RunWith(Parameterized.class)
    public static class WhitespaceStringParameter {
        @Parameterized.Parameter
        public String param_Whitespace;
        private ServiceFactory serviceFactory;
        private UserService userService;
        private EntityService entityService;
        
        private Collection<Permission> correctPermissions;
        private String correctGroupName;
        private Group correctGroup;
        private String correctUserName;
        private User correctUser;
        
        private String correctEntityName;
        private String correctEntityValue;
        
        @Parameterized.Parameters(name = "with \"{0}\"")
        public static Iterable<String> whitespaceStrings() {
            return Sets.newSet("", " ", "\t", "\n", "\r");
        }
        
        @Before
        public void prepare() {
            serviceFactory = new ServiceFactory();
            entityService = serviceFactory.createEntityService();
            userService = serviceFactory.createUserService();
            
            correctPermissions = Arrays.asList(Permission.read, Permission.write);
            correctGroupName = "group";
            correctGroup = userService.createGroup(correctGroupName, correctPermissions);
            correctUserName = "user";
            correctUser = userService.createUser(correctUserName, correctGroup);
            
            correctEntityName = "entity";
            correctEntityValue = "value";
            
        }
        
        @Test
        public void testCreateEntity_withWhitespaceName_mustReturnFalse() {
            Assert.assertFalse(entityService.createEntity(correctUser, param_Whitespace, correctEntityValue));
        }
        
        @Test
        public void testCreateEntity_withWhitespaceInName_mustReturnFalse() {
            Assert.assertFalse(entityService.createEntity(correctUser,
                    correctEntityName + param_Whitespace, correctEntityValue));
        }
        
    }
    
    public static class WithoutParameters {
        private ServiceFactory serviceFactory;
        private UserService userService;
        private EntityService entityService;
        
        private Collection<Permission> correctPermissions;
        private String correctGroupName;
        private Group correctGroup;
        private String correctUserName;
        private User correctUser;
        
        private String correctEntityName;
        private String correctEntityName32;
        private String correctEntityValue;
        
        
        @Before
        public void prepare() {
            serviceFactory = new ServiceFactory();
            entityService = serviceFactory.createEntityService();
            userService = serviceFactory.createUserService();
            
            correctPermissions = Arrays.asList(Permission.read, Permission.write);
            correctGroupName = "group";
            correctGroup = userService.createGroup(correctGroupName, correctPermissions);
            correctUserName = "user";
            correctUser = userService.createUser(correctUserName, correctGroup);
            
            correctEntityName = "entity";
            correctEntityName32 = "0123456789" + "0123456789" + "0123456789" + "01";
            Assert.assertEquals(correctEntityName32.length(), 32);
            correctEntityValue = "value";
            
        }
        
        @Test
        public void testCreateEntity_withNullUser_mustReturnFalse() {
            Assert.assertFalse(entityService.createEntity(null, correctEntityName,
                    correctEntityValue));
        }
        
        @Test
        public void testCreateEntity_withNullName_mustReturnFalse() {
            Assert.assertFalse(entityService.createEntity(correctUser, null, correctEntityValue));
        }
        
        @Test
        public void testCreateEntity_withNullValue_mustReturnFalse() {
            Assert.assertFalse(entityService.createEntity(correctUser, correctEntityName, null));
        }
        
        @Test
        public void testCreateEntity_nameLength32Limit() {
            Assert.assertTrue(entityService.createEntity(correctUser,
                    correctEntityName32, correctEntityValue));
            Assert.assertFalse(entityService.createEntity(correctUser,
                    correctEntityName32 + "a", correctEntityValue));
        }
        
        @Test
        public void testCreateEntity_withCorrectArguments_mustReturnTrue() {
            Assert.assertTrue(entityService.createEntity(correctUser, correctEntityName,
                    correctEntityValue));
        }
        
        @Test
        public void testGetEntityValue_withCorrectArguments_mustReturnCorrectValue() {
            entityService.createEntity(correctUser, correctEntityName, correctEntityValue);
            Assert.assertEquals(entityService.getEntityValue(correctUser, correctEntityName), correctEntityValue);
        }
        
        @Test
        public void testGetEntityValue_withWrongEntityName_mustReturnNull() {
            entityService.createEntity(correctUser, correctEntityName, correctEntityValue);
            Assert.assertNull(entityService.getEntityValue(correctUser, null));
            Assert.assertNull(entityService.getEntityValue(correctUser, correctEntityName + "1"));
        }
        
        @Test
        public void testGetEntityValue_withNullUser_mustReturnNull() {
            entityService.createEntity(correctUser, correctEntityName, correctEntityValue);
            Assert.assertNull(entityService.getEntityValue(null, correctEntityName));
        }
        
        @Test
        public void testGetEntityValue_withOwnerUser_withoutReadPermission_mustReturnCorrectValue() {
            Group group = userService.createGroup(correctGroupName + "2", Arrays.asList(Permission.write));
            User user = userService.createUser(correctUserName + "2", group);
            
            entityService.createEntity(user, correctEntityName, correctEntityValue);
            
            Assert.assertEquals(entityService.getEntityValue(user, correctEntityName), correctEntityValue);
        }
        
        @Test
        public void testGetEntityValue_withOwnerUser_withReadPermission_mustReturnCorrectValue() {
            Group group = userService.createGroup(correctGroupName + "2", Arrays.asList(Permission.read));
            User user = userService.createUser(correctUserName + "2", group);
            
            entityService.createEntity(user, correctEntityName, correctEntityValue);
            
            Assert.assertEquals(entityService.getEntityValue(user, correctEntityName), correctEntityValue);
        }
        
        @Test
        public void testGetEntityValue_withAnotherUser_withSameGroup_withoutReadPermission_mustReturnNull() {
            Group group = userService.createGroup(correctGroupName + "2", Arrays.asList(Permission.write));
            User user = userService.createUser(correctUserName + "2", group);
            User anotherUser = userService.createUser(correctUserName + "3", group);
            
            entityService.createEntity(user, correctEntityName, correctEntityValue);
            
            Assert.assertNull(entityService.getEntityValue(anotherUser, correctEntityName));
        }
        
        @Test
        public void testGetEntityValue_withAnotherUser_withSameGroup_withReadPermission_mustReturnCorrectValue() {
            Group group = userService.createGroup(correctGroupName + "2", Arrays.asList(Permission.read));
            User user = userService.createUser(correctUserName + "2", group);
            User anotherUser = userService.createUser(correctUserName + "3", group);
            
            entityService.createEntity(user, correctEntityName, correctEntityValue);
            
            Assert.assertEquals(entityService.getEntityValue(anotherUser, correctEntityName), correctEntityValue);
        }
        
        @Test
        public void testGetEntityValue_withAnotherUser_inAnotherGroup_withReadPermission_mustReturnNull() {
            Group group = userService.createGroup(correctGroupName + "2", Arrays.asList(Permission.read));
            User user = userService.createUser(correctUserName + "2", group);
            Group anotherGroup = userService.createGroup(correctGroupName + "3", Arrays.asList(Permission.read));
            User anotherUser = userService.createUser(correctUserName + "3", anotherGroup);
            
            entityService.createEntity(user, correctEntityName, correctEntityValue);
            
            Assert.assertNull(entityService.getEntityValue(anotherUser, correctEntityName));
        }
        
        @Test
        // Если пользователь состоит в одной из групп в которую входит создатель сущности,
        // он также имеет доступ на чтение/запись к этой сущности (в зависимости от разрешения этой группу)
        // (тоесть после создания сущности оба вступили в новую группу - доступ к сущности появился?)
        public void testGetEntityValue_withAnotherUser_inAnotherSameGroupAsOwner_withReadPermission_mustReturnCorrectValue() {
            Group group = userService.createGroup(correctGroupName + "2", Arrays.asList(Permission.read));
            User user = userService.createUser(correctUserName + "2", group);
            Group anotherGroup = userService.createGroup(correctGroupName + "3", Arrays.asList(Permission.read));
            User anotherUser = userService.createUser(correctUserName + "3", anotherGroup);
            Group emptyGroup = userService.createGroup(correctGroupName + "4", Arrays.asList(Permission.read));
            
            entityService.createEntity(user, correctEntityName, correctEntityValue);
            emptyGroup.addUser(user);
            emptyGroup.addUser(anotherUser);
            
            Assert.assertEquals(entityService.getEntityValue(anotherUser, correctEntityName), correctEntityValue);
        }
        
        @Test
        public void testGetEntityValue_withAnotherUser_inAnotherSameGroupAsOwner_withoutReadPermission_mustReturnNull() {
            Group group = userService.createGroup(correctGroupName + "2", Arrays.asList(Permission.read));
            User user = userService.createUser(correctUserName + "2", group);
            Group anotherGroup = userService.createGroup(correctGroupName + "3", Arrays.asList(Permission.read));
            User anotherUser = userService.createUser(correctUserName + "3", anotherGroup);
            Group emptyGroup = userService.createGroup(correctGroupName + "4", Arrays.asList(Permission.write));
            
            entityService.createEntity(user, correctEntityName, correctEntityValue);
            emptyGroup.addUser(user);
            emptyGroup.addUser(anotherUser);
            
            Assert.assertNull(entityService.getEntityValue(anotherUser, correctEntityName));
        }
        
        @Test
        public void testUpdateEntity_withCorrectArguments_mustReturnTrueAndUpdated() {
            entityService.createEntity(correctUser, correctEntityName, correctEntityValue);
            Assert.assertTrue(entityService.updateEntity(correctUser, correctEntityName, correctEntityValue + "a"));
            Assert.assertEquals(entityService.getEntityValue(correctUser, correctEntityName), correctEntityValue + "a");
        }
        
        @Test
        public void testUpdateEntity_withNullUser_mustReturnFalseAndNotUpdated() {
            entityService.createEntity(correctUser, correctEntityName, correctEntityValue);
            Assert.assertFalse(entityService.updateEntity(null, correctEntityName, correctEntityValue + "a"));
            Assert.assertEquals(entityService.getEntityValue(correctUser, correctEntityName), correctEntityValue);
        }
        
        @Test
        public void testUpdateEntity_withWrongEntityName_mustReturnFalseAndNotUpdated() {
            entityService.createEntity(correctUser, correctEntityName, correctEntityValue);
            Assert.assertFalse(entityService.updateEntity(correctUser, null, correctEntityValue + "a"));
            Assert.assertEquals(entityService.getEntityValue(correctUser, correctEntityName), correctEntityValue);
            Assert.assertFalse(entityService.updateEntity(correctUser, correctEntityName + "1", correctEntityValue + "a"));
            Assert.assertEquals(entityService.getEntityValue(correctUser, correctEntityName), correctEntityValue);
        }
        
        @Test
        public void testUpdateEntity_withOwnerUser_withoutWritePermission_mustReturnTrueAndUpdated() {
            Group group = userService.createGroup(correctGroupName + "2", Arrays.asList(Permission.read));
            User user = userService.createUser(correctUserName + "2", group);
            
            entityService.createEntity(user, correctEntityName, correctEntityValue);
            Assert.assertTrue(entityService.updateEntity(user, correctEntityName, correctEntityValue + "a"));
            Assert.assertEquals(entityService.getEntityValue(user, correctEntityName), correctEntityValue + "a");
        }
        
        @Test
        public void testUpdateEntity_withOwnerUser_withWritePermission_mustReturnTrueAndUpdated() {
            Group group = userService.createGroup(correctGroupName + "2", Arrays.asList(Permission.read));
            User user = userService.createUser(correctUserName + "2", group);
            
            entityService.createEntity(user, correctEntityName, correctEntityValue);
            Assert.assertTrue(entityService.updateEntity(user, correctEntityName, correctEntityValue + "a"));
            Assert.assertEquals(entityService.getEntityValue(user, correctEntityName), correctEntityValue + "a");
        }
        
        @Test
        public void testUpdateEntity_withAnotherUser_withSameGroup_withoutWritePermission_mustReturnFalseAndNotUpdated() {
            Group group = userService.createGroup(correctGroupName + "2", Arrays.asList(Permission.read));
            User user = userService.createUser(correctUserName + "2", group);
            User anotherUser = userService.createUser(correctUserName + "3", group);
            
            entityService.createEntity(user, correctEntityName, correctEntityValue);
            Assert.assertFalse(entityService.updateEntity(anotherUser, correctEntityName, correctEntityValue + "a"));
            Assert.assertEquals(entityService.getEntityValue(user, correctEntityName), correctEntityValue);
        }
        
        @Test
        public void testUpdateEntity_withAnotherUser_withSameGroup_withWritePermission_mustReturnTrueAndUpdated() {
            Group group = userService.createGroup(correctGroupName + "2", Arrays.asList(Permission.write));
            User user = userService.createUser(correctUserName + "2", group);
            User anotherUser = userService.createUser(correctUserName + "3", group);
            
            entityService.createEntity(user, correctEntityName, correctEntityValue);
            Assert.assertTrue(entityService.updateEntity(anotherUser, correctEntityName, correctEntityValue + "a"));
            Assert.assertEquals(entityService.getEntityValue(user, correctEntityName), correctEntityValue + "a");
        }
        
        @Test
        public void testUpdateEntity_withAnotherUser_inAnotherGroup_withWritePermission_mustReturnFalseAndNotUpdated() {
            Group group = userService.createGroup(correctGroupName + "2", Arrays.asList(Permission.write));
            User user = userService.createUser(correctUserName + "2", group);
            Group anotherGroup = userService.createGroup(correctGroupName + "3", Arrays.asList(Permission.write));
            User anotherUser = userService.createUser(correctUserName + "3", anotherGroup);
            
            entityService.createEntity(user, correctEntityName, correctEntityValue);
            Assert.assertFalse(entityService.updateEntity(anotherUser, correctEntityName, correctEntityValue + "a"));
            Assert.assertEquals(entityService.getEntityValue(user, correctEntityName), correctEntityValue);
        }
        
        @Test
        // Если пользователь состоит в одной из групп в которую входит создатель сущности,
        // он также имеет доступ на чтение/запись к этой сущности (в зависимости от разрешения этой группу)
        public void testUpdateEntity_withAnotherUser_inAnotherSameGroupAsOwner_withWritePermission_mustReturnTrueAndUpdated() {
            Group group = userService.createGroup(correctGroupName + "2", Arrays.asList(Permission.read));
            User user = userService.createUser(correctUserName + "2", group);
            Group anotherGroup = userService.createGroup(correctGroupName + "3", Arrays.asList(Permission.read));
            User anotherUser = userService.createUser(correctUserName + "3", anotherGroup);
            Group emptyGroup = userService.createGroup(correctGroupName + "4", Arrays.asList(Permission.read));
            
            entityService.createEntity(user, correctEntityName, correctEntityValue);
            emptyGroup.addUser(user);
            emptyGroup.addUser(anotherUser);
            
            entityService.createEntity(user, correctEntityName, correctEntityValue);
            Assert.assertTrue(entityService.updateEntity(anotherUser, correctEntityName, correctEntityValue + "a"));
            Assert.assertEquals(entityService.getEntityValue(user, correctEntityName), correctEntityValue + "a");
        }
        
        @Test
        public void testUpdateEntity_withAnotherUser_inAnotherSameGroupAsOwner_withoutWritePermission_mustReturnFalseAndNotUpdated() {
            Group group = userService.createGroup(correctGroupName + "2", Arrays.asList(Permission.read));
            User user = userService.createUser(correctUserName + "2", group);
            Group anotherGroup = userService.createGroup(correctGroupName + "3", Arrays.asList(Permission.read));
            User anotherUser = userService.createUser(correctUserName + "3", anotherGroup);
            Group emptyGroup = userService.createGroup(correctGroupName + "4", Arrays.asList(Permission.write));
            
            entityService.createEntity(user, correctEntityName, correctEntityValue);
            emptyGroup.addUser(user);
            emptyGroup.addUser(anotherUser);
            
            entityService.createEntity(user, correctEntityName, correctEntityValue);
            Assert.assertFalse(entityService.updateEntity(anotherUser, correctEntityName, correctEntityValue + "a"));
            Assert.assertEquals(entityService.getEntityValue(user, correctEntityName), correctEntityValue);
        }
    }
}
