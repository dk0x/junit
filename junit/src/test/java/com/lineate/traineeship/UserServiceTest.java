package com.lineate.traineeship;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.internal.util.collections.Sets;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

@RunWith(Enclosed.class)
public class UserServiceTest {
    @RunWith(Parameterized.class)
    public static class WhitespaceStringParameter {
        @Parameterized.Parameter
        public String param_Whitespace;
        private ServiceFactory serviceFactory;
        private UserService userService;
        private Collection<Permission> correctPermissions;
        private String correctGroupName;
        
        @Parameterized.Parameters(name = "with \"{0}\"")
        public static Iterable<String> whitespaceStrings() {
            return Sets.newSet("", " ", "\t", "\n", "\r");
        }
        
        @Before
        public void prepare() {
            serviceFactory = new ServiceFactory();
            userService = serviceFactory.createUserService();
            correctPermissions = Arrays.asList(Permission.read, Permission.write);
            correctGroupName = "group";
        }
        
        @Test
        public void testCreateGroup_withWhitespaceName_mustReturnNull() {
            Assert.assertNull(userService.createGroup(param_Whitespace, correctPermissions));
        }
        
        @Test
        public void testCreateUser_withWhitespaceName_mustReturnNull() {
            Group correctGroup = userService.createGroup(correctGroupName, correctPermissions);
            
            Assert.assertNull(userService.createUser(param_Whitespace, correctGroup));
        }
        
    }
    
    public static class WithoutParameters {
        private ServiceFactory serviceFactory;
        private UserService userService;
        private Collection<Permission> correctPermissions;
        private String correctGroupName;
        private String correctUserName;
        
        
        @Before
        public void prepare() {
            serviceFactory = new ServiceFactory();
            userService = serviceFactory.createUserService();
            correctPermissions = Arrays.asList(Permission.read, Permission.write);
            correctGroupName = "group";
            correctUserName = "user";
        }
        
        @Test
        public void testCreateGroup_withNullOrEmptyPermissions_mustReturnNull() {
            Assert.assertNull(userService.createGroup(correctGroupName, null));
            Assert.assertNull(userService.createGroup(correctGroupName, new ArrayList<>()));
        }
        
        @Test
        public void testCreateGroup_withCorrectArguments_mustReturnGroupWithEqualsFields() {
            Group group = userService.createGroup(correctGroupName, correctPermissions);
            
            Assert.assertNotNull(group);
            Assert.assertEquals(group.getName(), correctGroupName);
            Assert.assertEquals(group.getPermissions(), correctPermissions);
        }
        
        @Test
        public void testCreateGroup_withExistName_mustReturnNull() { // that true?
            Group group = userService.createGroup(correctGroupName, correctPermissions);
            
            Assert.assertNull(userService.createGroup(correctGroupName, correctPermissions));
        }
        
        @Test
        public void testCreateGroup_withNullName_mustReturnNull() {
            Assert.assertNull(userService.createGroup(null, correctPermissions));
        }
        
        
        @Test
        public void testGroup_addUserGetUser_mustEquals() {
            Group correctGroup = userService.createGroup(correctGroupName, correctPermissions);
            Group correctGroup2 = userService.createGroup(correctGroupName + "2", correctPermissions);
            User correctUser = userService.createUser(correctUserName, correctGroup);
            
            Assert.assertEquals(correctGroup2.getUsers().size(), 0);
            Assert.assertFalse(correctGroup2.getUsers().contains(correctUser));
            correctGroup2.addUser(correctUser);
            Assert.assertTrue(correctGroup2.getUsers().contains(correctUser));
            Assert.assertEquals(correctGroup2.getUsers().size(), 1);
        }
        
        
        @Test
        public void testCreateUser_withNullGroup_mustReturnNull() {
            Assert.assertNull(userService.createUser(correctUserName, null));
        }
        
        @Test
        public void testCreateUser_withNullName_mustReturnNull() {
            Group correctGroup = userService.createGroup(correctGroupName, correctPermissions);
            
            Assert.assertNull(userService.createUser(null, correctGroup));
        }
        
        
        @Test
        public void testCreateUser_withCorrectArguments_mustReturnUserWithEqualsFields() {
            Group correctGroup = userService.createGroup(correctGroupName, correctPermissions);
            User user = userService.createUser(correctUserName, correctGroup);
            
            Assert.assertNotNull(user);
            Assert.assertEquals(user.getName(), correctUserName);
            Assert.assertTrue(user.getGroups().contains(correctGroup));
        }
        
        @Test
        public void testCreateUser_withExistName_mustReturnNull() {
            Group correctGroup = userService.createGroup(correctGroupName, correctPermissions);
            User user = userService.createUser(correctUserName + "2", correctGroup);
            
            Assert.assertNull(userService.createUser(correctUserName + "2", correctGroup));
        }
    }
}
